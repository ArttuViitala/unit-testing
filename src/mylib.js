const mylib = {
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    substract: (a, b) => {
        return a - b;
    },
    divide: function (dividend, divisor) {
        if (divisor === 0) {
            throw new Error("Cannot divide by zero");
        };
        return dividend / divisor;
    },
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;