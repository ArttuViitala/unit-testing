const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // initialization
        // create objects... etc
        console.log("Initialising tests.");
    });
    it("Can add 1 and 2 together", () => {
        // Tests
        expect(mylib.add(1,2)).equal(3, "1+2 is not 3, for some reason?");
    });
    it("Can divide 6 with 2", () => {
        expect(mylib.divide(6,2)).equal(3, "6/2 is not 3, for some reason?");
    });
    it("Should return an error when dividing by 0", () => {
        expect(() => {
            mylib.divide(5,0);
        }).Throw("Cannot divide by zero");
    });
    it("Can substract 5 from 3", () => {
        expect(mylib.substract(5,3)).equal(2, "5-3 is not 2, for some reason?");
    });
    it("Can multiply 5 with 5", () => {
        expect(mylib.multiply(5,5)).equal(25, "5*5 is not 25, for some reason?");
    });
    after(() => {
        // Cleanup
        // For example: shut express server down.
        console.log("Testing completed!")
    });
});